import tensorflow as tf
from time import time as tt
import numpy as np

def produceData(w_array, num):
    X = []
    Y = []
    for _ in range(num):
        x = np.random.random(size=len(w_array))
        y = sum(x * w_array)
        flag = 1 if y > 5 else -1
        flag = 0
        y += flag * 0.01
        X.append(x)
        Y.append(y)
    return X, Y

X, Y = produceData([1, 2, 3, 4], 500)
X = np.array(X).reshape(500, 4, 1)
Y = np.array(Y)

# XV, YV means validation data
XV, YV = produceData([1, 2, 3, 4], 30)
XV = np.array(XV).reshape(30, 4, 1)
YV = np.array(YV)

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(4, 1)),
  # if use these layers
  # uder the same steps, the mae is bigger
  #tf.keras.layers.Dense(128, activation='linear'),
  #tf.keras.layers.Dropout(0.2),
  #tf.keras.layers.Dense(64, activation='linear'),
  #tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(1, activation='linear')]
    )

model.compile(optimizer="sgd",
              loss='mean_squared_error',
              metrics=['mae']
             )

model.fit(X, Y, epochs=300,
          validation_data=(XV, YV))

model.evaluate(XV, YV, verbose=2)

r = model.predict(XV)
print(r)
print(YV)
