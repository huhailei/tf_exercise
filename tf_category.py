import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Flatten
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

def produce_helix_data(count, rounds, start_from=0, r_multiple=2):
    theta = np.linspace(start_from + 0.001, np.pi * 2 * rounds + start_from, count)
    r = r_multiple * (theta - start_from)
    return np.vstack((r * np.cos(theta), r * np.sin(theta)))

def produce_data(num, rounds=2):
    count = int(num / 2)
    x1 = produce_helix_data(count, rounds, 0)
    e1 = np.random.normal(0, 0.5, x1.shape)
    x1 = x1 + e1
    x2 = produce_helix_data(num - count, rounds, np.pi)
    e2 = np.random.normal(0, 0.1, x2.shape)
    x2 = x2 + e1
    print(x1.shape)
    # plt.plot(x1[0], x1[1])
    # plt.plot(x2[0], x2[1])
    # plt.show()
    y1 = np.ones(count)
    y2 = np.zeros(num - count)
    X = np.hstack((x1, x2)).T.reshape(num, 2, 1)
    Y = np.hstack((y1, y2)).reshape(num, 1)
    return train_test_split(X, Y, random_state=22)

def produceData(r,w,d,num): 
    r1 = r-w/2
    r2 = r+w/2
    #上半圆 
    theta1 = np.random.uniform(0, np.pi ,num) 
    X_Col1 = np.random.uniform( r1*np.cos(theta1),r2*np.cos(theta1),num)[:, np.newaxis] 
    X_Row1 = np.random.uniform(r1*np.sin(theta1),r2*np.sin(theta1),num)[:, np.newaxis] 
    Y_label1 = np.ones(num) #类别标签为1 
    #下半圆 
    theta2 = np.random.uniform(-np.pi, 0 ,num) 
    X_Col2 = (np.random.uniform( r1*np.cos(theta2),r2*np.cos(theta2),num) + r)[:, np.newaxis] 
    X_Row2 = (np.random.uniform(r1 * np.sin(theta2), r2 * np.sin(theta2), num) -d)[:,np.newaxis] 
    Y_label2 = np.zeros(num) #类别标签为-1,注意：由于采取双曲正切函数作为激活函数，类别标签不能为0 
    #合并 
    X_Col = np.vstack((X_Col1, X_Col2)) 
    X_Row = np.vstack((X_Row1, X_Row2)) 
    X = np.hstack((X_Col, X_Row)).reshape(num * 2, 2, 1)
    Y_label = np.hstack((Y_label1,Y_label2)) 
    Y_label.shape = (num*2 , 1) 

    return train_test_split(X, Y_label, random_state=22)

if __name__ == "__main__":
    epochs = 200
    batch_size = 32
    num_classes = 1
    # X_train, X_test, y_train, y_test = produceData(10, 5, -1,5000)
    X_train, X_test, y_train, y_test = produce_data(10000, 3)
    # convert class vectors to binary class matrices
    # y_train = tf.keras.utils.to_categorical(y_train, num_classes)
    # y_test = tf.keras.utils.to_categorical(y_test, num_classes)
    model = tf.keras.models.Sequential([
        tf.keras.layers.Flatten(input_shape=(2, 1)),
        tf.keras.layers.Dense(768, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(512, activation='tanh'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(160, activation='tanh'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(64, activation='tanh'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(2, activation='sigmoid')
    ])

    model.compile(optimizer='Adam',
                loss='sparse_categorical_crossentropy',
                metrics=['accuracy']
                )
    model.fit(X_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(X_test, y_test))
    score = model.evaluate(X_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])