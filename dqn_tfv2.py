#######################################################################
# Copyright (C)                                                       #
# 2016 - 2019 Pinard Liu(liujianping-ok@163.com)                      #
# https://www.cnblogs.com/pinard                                      #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################
##https://www.cnblogs.com/pinard/p/9714655.html ##
## 强化学习（八）价值函数的近似表示与Deep Q-Learning ##

import gym, time
import tensorflow as tf
import numpy as np
import random
from collections import deque
import gc
from itertools import chain

# Hyper Parameters for DQN
GAMMA = 0.9 # discount factor for target Q
INITIAL_EPSILON = 0.4 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon
REPLAY_SIZE = 10000 # experience replay buffer size
BATCH_SIZE = 512 # size of minibatch

class DQN():
  # DQN Agent
  def __init__(self, env):
    # init experience replay
    self.replay_buffer = deque()
    # init some parameters
    self.time_step = 0
    self.epsilon = INITIAL_EPSILON
    self.state_dim = env.observation_space.shape[0]
    self.action_dim = env.action_space.n

    self.create_Q_network()

  def create_Q_network(self):
    model = tf.keras.models.Sequential([
      tf.keras.layers.Flatten(input_shape=(self.state_dim + self.action_dim, 1)),
      # if use these layers
      # uder the same steps, the mae is bigger
      #tf.keras.layers.Dense(128, activation='linear'),
      #tf.keras.layers.Dropout(0.2),
      tf.keras.layers.Dense(64, activation='relu'),
      tf.keras.layers.Dropout(0.2),
      tf.keras.layers.Dense(1, activation='linear')]
    )

    model.compile(optimizer="Adam",
                  loss='mean_squared_error',
                  metrics=['mae']
                )
    self.model = model

  def perceive(self,state,action,reward,next_state,done):
    one_hot_action = np.zeros(self.action_dim)
    one_hot_action[action] = 1
    self.time_step += 1
    buffer = self.replay_buffer
    buffer.append((state, one_hot_action, reward, next_state, done))
    if len(buffer) > REPLAY_SIZE:
      buffer.popleft()
    del buffer

    if self.time_step > BATCH_SIZE:
      self.time_step = 0
      self.train_Q_network()

  def predict_for_state(self, state):
    x_raw = []
    for i in range(self.action_dim):
      one_hot_action = np.zeros(self.action_dim)
      one_hot_action[i] = 1
      x_raw.append([j for j in chain(one_hot_action, state)])
    x_input = np.array(x_raw).reshape(self.action_dim, self.action_dim + self.state_dim, 1)
    predictions = self.model.predict(x_input) # [[predict_result_0], [predict_result_1]]
    return [p[0] for p in predictions]

  def predict_Q_for_states(self, states):
    x_raw = []
    action_dim = self.action_dim
    for s in states:
      for i in range(action_dim):
        one_hot_action = np.zeros(action_dim)
        one_hot_action[i] = 1
        x_raw.append([j for j in chain(one_hot_action, s)])
    x_input = np.array(x_raw).reshape(action_dim * len(states), action_dim + self.state_dim, 1)
    predictions = self.model.predict(x_input)
    return [np.max([predictions[action_dim * i + j][0] for j in range(action_dim)]) for i in range(len(states))]

  def train_Q_network(self):
    # Step 1: obtain random minibatch from replay memory
    minibatch = random.sample(self.replay_buffer,BATCH_SIZE)

    y_raw = [] # reward
    x_raw = [] # state and action
    next_states = [data[3] for data in minibatch]
    next_Q_values = self.predict_Q_for_states(next_states)
    for (state, action, reward, next_state, done), next_Q in zip(minibatch, next_Q_values):
      x_raw.append([i for i in chain(action, state)])
      if done:
        y_raw.append(reward)
      else:
        y_raw.append(reward + GAMMA * next_Q)
    x_batch = np.array(x_raw).reshape(BATCH_SIZE, self.action_dim + self.state_dim, 1)
    y_batch = np.array(y_raw)

    # Step 2: training the model
    self.model.fit(x_batch, y_batch, epochs=25, verbose=0)
    # weights = [i.numpy() for i in self.model.weights]
    # del self.model
    # del minibatch
    # del x_batch
    # del y_batch
    # del x_raw
    # del y_raw
    # self.model = None
    # self.create_Q_network()
    # self.model.set_weights(weights)
    # del weights
    # time.sleep(0.01)
    # gc.collect()

  def egreedy_action(self,state):
    if random.random() <= self.epsilon:
        self.epsilon -= (INITIAL_EPSILON - FINAL_EPSILON) / 10000
        return random.randint(0, self.action_dim - 1) # self.action_dim - 1 = 1
    else:
        self.epsilon -= (INITIAL_EPSILON - FINAL_EPSILON) / 10000
        return np.argmax(self.predict_for_state(state))

  def action(self,state):
    return np.argmax(self.predict_for_state(state))

  def weight_variable(self,shape):
    initial = tf.random.truncated_normal(shape)
    return tf.Variable(initial)

  def bias_variable(self,shape):
    initial = tf.constant(0.01, shape = shape)
    return tf.Variable(initial)
# ---------------------------------------------------------
# Hyper Parameters
ENV_NAME = 'CartPole-v0'
EPISODE = 3000 # Episode limitation
STEP = 300 # Step limitation in an episode
TEST = 10 # The number of experiment test every 100 episode

def main():
  # initialize OpenAI Gym env and dqn agent
  env = gym.make(ENV_NAME)
  agent = DQN(env)
  start_time = time.time()
  for episode in range(EPISODE):
    # initialize task
    state = env.reset()
    # Train
    for step in range(1, STEP):
      env.render()
      action = agent.egreedy_action(state) # e-greedy action for train
      next_state, reward, done, _ = env.step(action)
      # Define reward for agent
      reward = -1 if done and step < 200 else 0.1
      agent.perceive(state, action, reward, next_state, done)
      state = next_state
      del _
      if done:
        break
    # Test every 100 episodes
    if episode % 100 == 0:
      total_reward = 0
      for i in range(TEST):
        state = env.reset()
        for j in range(STEP):
          env.render()
          action = agent.action(state) # direct action for test
          state,reward,done,_ = env.step(action)
          total_reward += reward
          if done:
            break
      ave_reward = total_reward/TEST
      end_time = time.time()
      print ('episode: ',episode,'Evaluation AveReward:%.1f, Consumed time = %.2f' % (ave_reward, end_time - start_time))
      start_time = end_time

if __name__ == '__main__':
  main()
