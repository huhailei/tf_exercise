import subprocess, random, sys, datetime
import logging, tqdm, time, argparse
import tushare as ts
import baostock as bs
import psycopg2 as psy
import multiprocessing as mp
from multiprocessing import Pool

parser = argparse.ArgumentParser(description='Used to cache stock data to postgresql.')
parser.add_argument('--toponly', '-t', help='取值1,0；仅缓存top 股票包括hs300和zz500，非必要参数，缺省值0.', default=0)
args = parser.parse_args()

"""
create table stocks_d (
    date timestamp,
    code char(9),
    open float8,
    high float8,
    low float8,
    close float8,
    preclose float8,
    volume bigint,
    amount float8,
    adjustflag smallint, 复权状态	不复权、前复权、后复权
    turn float8,
    tradestatus smallint, 1：正常交易 0：停牌
    pctChg float8,
    peTTM float8,
    psTTM float8,
    pcfNcfTTM float8,
    pbMRQ float8,
    isST smallint, 1是，0否
    primary key (date, code)
);
"""

"""
create table if not exists sh.601588 (
    date timestamp,
    code char(9),
    type smallint, 5, 15, 30, 60
    open float8,
    high float8,
    low float8,
    close float8,
    volume bigint,
    amount float8,
    adjustflag smallint, 复权状态	不复权、前复权、后复权
    primary key(date, type)
);
"""

def get_available_addres():
    ips = ["119.147.212.81", "14.215.128.18"] # ips used in pytdx
    for i in range(5):
        cnts = " -c 4 " if sys.platform != 'win32' else " "
        ip = random.choice(ips)
        data = subprocess.run(f"ping {cnts} {ip} &>/dev/null", shell=True)
        if data.returncode == 0:
            return ip
    return None

def get_all_codes():
    bs.login()
    astks = bs.query_stock_basic()
    bs.logout()
    return [i[0] for i in astks.data if i[0].startswith('sh.6') or i[0].startswith('sz.0') or i[0].startswith('sz.3')]

def get_hs300_codes():
    bs.login()
    hs300 = bs.query_hs300_stocks()
    bs.logout()
    return [d[1] for d in hs300.data]

def get_zz500_codes():
    bs.login()
    zz500 = bs.query_zz500_stocks()
    bs.logout()
    return [d[1] for d in zz500.data]

def get_top_codes():
    return get_hs300_codes() + get_zz500_codes()

def make_sure_main_table_is_created():
    qs = """
    create table if not exists stocks_d (
        date timestamp,
        code char(9),
        open float8,
        high float8,
        low float8,
        close float8,
        preclose float8,
        volume bigint,
        amount float8,
        adjustflag smallint,
        turn float8,
        tradestatus smallint,
        pctChg float8,
        peTTM float8,
        psTTM float8,
        pcfNcfTTM float8,
        pbMRQ float8,
        isST smallint,
        primary key (date, code)
    );
    """
    con = dbi()
    cur = con.cursor()
    cur.execute(qs)
    con.commit()
    con.close()

def table_name_for_code(code):
    return code.replace(".", "_")

def make_sure_table_is_created(code): # sh.601588, sz.000725
    qs = f"""
        create table if not exists {table_name_for_code(code)} (
            date timestamp,
            code char(9),
            type smallint,
            open float8,
            high float8,
            low float8,
            close float8,
            volume bigint,
            amount float8,
            adjustflag smallint,
            primary key(date, type)
        );
        """
    con = dbi()
    cur = con.cursor()
    cur.execute(qs)
    con.commit()
    con.close()

def dbi(dbname='stocks'):
    user="postgres"
    password="loveming"
    host="127.0.0.1"
    port="5432"
    con = psy.connect("dbname={} host={} \
    user={} password={} port={}".format(dbname, host, user, password, port))
    return con

def date_from_c_time(c_time):
    # check if the date is valid
    result = ''
    try:
        dt = c_time + datetime.timedelta(days=1)
        result = datetime.datetime.strftime(dt, "%Y-%m-%d")
    except ValueError as error:
        logging.error("Error happed to convert date_from_c_time {}, error:{}".format(c_time, str(error)))
        result = ' '
    return result

def fetch_k_info_for(code, try_cnt=3):
    if try_cnt <= 0:
        return
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(message)s',
                    level=logging.INFO)
    # 1. try to get last date
    start_time = time.time()
    start_date = '1990-12-19'
    try:
        qs = f"select max(date) from stocks_d where code='{code}';"
        con = dbi()
        cur = con.cursor()
        cur.execute(qs)
        row = cur.fetchone()
        if isinstance(row[0], datetime.datetime):
            start_date = date_from_c_time(row[0])
    except Exception as e:
        con.close()
        logging.error(f"{code}: {repr(e)}")
        return
    bs.login()
    end_date = max(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d"), start_date)
    rs = bs.query_history_k_data_plus(code,
        "date,code,open,high,low,close,preclose,volume,amount,adjustflag,turn,tradestatus,pctChg,peTTM,psTTM,pcfNcfTTM,pbMRQ,isST",
        start_date=start_date, end_date=end_date, frequency="d", adjustflag="2")
    # print(rs)
    if rs.error_code != '0':
        # failed
        logging.error(f"{code}: {rs.error_code}-{rs.error_msg}- Prepare to try again at {try_cnt}.")
        con.close()
        bs.logout()
        fetch_k_info_for(code, try_cnt-1)
        return
    # succeeded 
    if len(rs.data) > 0:
        isql = f"insert into stocks_d values "
        def f(item):
            item = [ci if len(ci) > 0 else "0" for ci in item]
            return f"('{item[0]}','{item[1]}',{','.join(item[2:])})"
        isql += ','.join(map(f, rs.data))
        isql += ";"
        cur.execute(isql)
        con.commit()

    make_sure_table_is_created(code)
    # 2, go to get miniutes k data
    tn = table_name_for_code(code)
    for ms in [5, 15, 30, 60]:
        sql = f"select max(date) from {tn} where code='{code}' and type={ms};"
        cur.execute(sql)
        row = cur.fetchone()
        if not isinstance(row[0], datetime.datetime):
            sql = f"select min(date) from stocks_d where code='{code}';"
            cur.execute(sql)
            row = cur.fetchone()
            # print(row[0], type(row[0]))
            if not isinstance(row[0], datetime.datetime):
                # No data in daily table for code, just return
                logging.error(f"{code}: No data in daily table for code, just return")
                con.close()
                bs.logout()
                fetch_k_info_for(code, try_cnt-1)
                return
        st = row[0] + datetime.timedelta(hours=9) # last value should be like "2019-10-09 15:00:00"
        now = datetime.datetime.now()
        while st < now:
            et = st + datetime.timedelta(days=ms*20)
            start_date = datetime.datetime.strftime(st, "%Y-%m-%d")
            end_date = datetime.datetime.strftime(et, "%Y-%m-%d")
            rs = bs.query_history_k_data_plus(code,
                "date,time,code,open,high,low,close,volume,amount,adjustflag",
                start_date=start_date, end_date=end_date, frequency=str(ms), adjustflag="2")
            start_i = 0
            # find existed data and ignore them
            while start_i < len(rs.data):
                item = rs.data[start_i]
                dt = datetime.datetime.strptime(item[1], "%Y%m%d%H%M%S%f")
                sql = f"select max(date) from {tn} where date='{dt}' and type={ms};"
                cur.execute(sql)
                row = cur.fetchone()
                if not isinstance(row[0], datetime.datetime):
                    break
                start_i += 1
            # just insert new datas
            if start_i < len(rs.data):
                sql = f"insert into {tn} values "
                def f(item):
                    dt = datetime.datetime.strptime(item[1], "%Y%m%d%H%M%S%f")
                    return f"('{dt}', '{item[2]}', {ms}, " + ",".join(item[3:]) + ")"
                sql += ",".join(map(f, rs.data[start_i:]))
                cur.execute(sql)
                con.commit()
            st = et + datetime.timedelta(days=1)
    con.close()
    bs.logout()
    logging.info(f"{code}: finished downloading in {time.time() - start_time:.1f}s!")

def main(top_only):
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(message)s',
                        level=logging.INFO)
    try_cnt = 3
    should_try = True
    codes = []
    while should_try and try_cnt:
        try:
            if top_only:
                codes = get_top_codes()
                logging.info("We are going to cache hs300 and zz500 only.")
            else:
                codes = get_all_codes()
                logging.info("We are going to cache all stocks.")
            # codes = ["sz.000725"]
        except Exception as e:
            try_cnt -= 1
        should_try = False
    make_sure_main_table_is_created()
    pool = Pool(max(mp.cpu_count(), 4))
    pool.map(fetch_k_info_for, codes)
    pool.close()
    pool.join()
    
if __name__ == "__main__":
    main(args.toponly)
