import pandas as pd
import tushare as ts
import os
import time, datetime
import multiprocessing as mp
import psycopg2 as psy
from collections import namedtuple

"""
create table stocks_a (
    code char(6),
    date date,
    open float8,
    high float8,
    close float8,
    low float8,
    volume float8,
    price_change float8,
    p_change float8,
    ma5 float8,
    ma10 float8,
    ma20 float8,
    v_ma5 float8,
    v_ma10 float8,
    v_ma20 float8
);
"""

"""
create table stocks_5 (
    code char(6),
    c_time timestamp,
    open float8,
    high float8,
    close float8,
    low float8,
    volume float8,
    price_change float8,
    p_change float8,
    ma5 float8,
    ma10 float8,
    ma20 float8,
    v_ma5 float8,
    v_ma10 float8,
    v_ma20 float8,
    turnover float8
);
"""

INFO = namedtuple("INFO", ['db', 'table', 'ktype', 'code'], defaults=[''])

def generate_info():
    infos = []
    i1 = INFO("stocks", "stocks_D", "D")
    infos.append(i1)
    i2 = INFO("stocks", "stocks_5", "5")
    infos.append(i2)
    i2 = INFO("stocks", "stocks_15", "15")
    infos.append(i2)
    i2 = INFO("stocks", "stocks_30", "30")
    infos.append(i2)
    i2 = INFO("stocks", "stocks_60", "60")
    infos.append(i2)
    i2 = INFO("stocks", "stocks_W", "W")
    infos.append(i2)
    i2 = INFO("stocks", "stocks_M", "M")
    infos.append(i2)
    return infos

# variables
def dbi(dbname='stocks'):
    user="postgres"
    password="loveming"
    host="192.168.50.179"
    port="5432"
    db="stocks"
    con = psy.connect("dbname={} host={} \
    user={} password={} port={}".format(dbname, host, user, password, port))
    return con

def date_from_c_time(c_time):
    # check if the date is valid
    result = ''
    try:
        dt = c_time + datetime.timedelta(days=1)
        result = datetime.datetime.strftime(dt, "%Y-%m-%d")
    except ValueError as error:
        print("Error happed to convert date_from_c_time {}, error:{}".format(c_time, str(error)))
        result = ' '
    return result


def cache_stock_for(info):
    """
        info's type is INFO
    """    
    con = dbi(info.db)
    #1, try to fetch the data in db
    sql = "select max(c_time) from {} where code='{}';".format(info.table, info.code)
    cur = con.cursor()
    cur.execute(sql)
    row = cur.fetchone()
    start_date = ''
    if isinstance(row[0], datetime.datetime):
        start_date = date_from_c_time(row[0])
    #2, def the start:string 开始日期 format：YYYY-MM-DD 为空时取到API所提供的最早日期数据
    code = info.code
    try:
        stock = ts.get_hist_data(code, ktype=info.ktype, start=start_date, pause=1, retry_count=10)
    except Exception as e:
        print("get_hist_data failed for {} and error:{}".format(str(info), str(e)))
        stock = pd.DataFrame() # just to return
    if not isinstance(stock, pd.DataFrame) or stock.empty:
        con.close()
        return
    #3, generate the insert sql
    sql = 'insert into {} values '.format(info.table)
    #3.1, convert into csv format
    ss = stock.to_csv()
    #3.2 convert to many items
    sa = ss.split('\n')
    #3.3 abandon the first title row
    sa = sa[1:-1]
    #3.4 add (' to the timestamp value according to c_time in table.
    prefix = "('{}', '".format(code)
    sa = [prefix + i.replace(",", "',", 1) + ")" for i in sa]
    #3.5 finally generate the final sql
    sql += ",".join(sa)
    cur = con.cursor()
    cur.execute(sql)
    con.commit()
    con.close()

def generater_infos_codes(infos, codes):
    for code in codes:
        for i in infos:
            yield INFO(i.db, i.table, i.ktype, code)

def cache_stocks():
    sb = ts.get_stock_basics()
    codes = sb.index
    infos = generate_info()
    #1, check if the db and table exists
    for i in infos:
        query = """create table if not exists %s (
        code char(6),
        c_time timestamp,
        open float8,
        high float8,
        close float8,
        low float8,
        volume float8,
        price_change float8,
        p_change float8,
        ma5 float8,
        ma10 float8,
        ma20 float8,
        v_ma5 float8,
        v_ma10 float8,
        v_ma20 float8,
        turnover float8
        );""" % i.table
        con = dbi(i.db)
        cur = con.cursor()
        cur.execute(query)
        con.commit()
        con.close()
    #2, go to feth the stock data
    pool = mp.Pool(mp.cpu_count() * 3)
    pool.map(cache_stock_for, generater_infos_codes(infos, codes))
    pool.close()
    pool.join()

if __name__ == "__main__":
    st = time.time()
    cache_stocks()
    print("Finished! Consuming time = ", time.time() - st)
